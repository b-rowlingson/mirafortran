# MiraFortran

## Disclaimer

This repository contains "The ZX Spectrum Mira Fortran Compiler". This
dates from 1986 and if Mira Software or the copyright holder would like
this removed then I shall do so immediately. 

## Contents

The `Tape` directory contains a `tzx` archive suitable for most ZX Spectrum emulators. This is only side 1 of the tape - side 2 contains more copies of the compiler (which has to be loaded in from tape after any code is compiled and run).

The `Documentation` directory contains PDF scans of all the documentation I had with the tape, including the tape itself and the purchase receipt.

## Using the Compiler with an Emulator

In this section I will show you how to run one of the demo programs
on the tape using a ZX Spectrum emulator. 

 * Do a hard reset
 
![](images/screen_01.png)
 
 * Open TZX
 
![](images/screen_02.png)
 
 * Press J for "Load"
 
 * Enter name of the program, or press "Enter" for the next on the tape. This is FUNCT -- note the name is case sensitive.
 
![](images/screen_03.png)
 
 * Read the documentation PDF if you want to edit the code. 
 
 * Press X to compile. This uses screen RAM and so you'll see noise on screen, and the second part of the compiler will load from the tape image. When finished you'll see some text and a beep.
 
![](images/screen_04.png)
 
 * The code is now in high memory -- "above RAMTOP" -- which means a soft reset won't erase it. Press enter to reset.
 
 * Run with RANDOMIZE USR 63500 (RANDOMIZE is on T, USR is Extended shift L

![](images/screen_07.png)

 * The first function will now plot and solve for f(x) = 0, and integrate.

![](images/screen_05.png)

 * Continue for the second function.
 
![](images/screen_06.png)

* Do a hard reset and reload the compiler from scratch - repeat until your Fortran is debugged and running well!


